function hamClick()
{
    const heightOfLeftSection = document.getElementsByClassName("leftSection")[0].style.height;
    if(heightOfLeftSection == "40%") {
        document.getElementsByClassName("leftSection")[0].style.height = "0%";
        document.getElementById("arrowImage").src = "../images/down.png";
    } else {
        document.getElementsByClassName("leftSection")[0].style.height = "40%";
        document.getElementById("arrowImage").src = "../images/up.png";
    }
}

function setActive(child) {

    for(let i=0;i<6;++i) {
        if(i == child) {
            document.getElementsByClassName("leftDesktopOption")[i].className = "leftDesktopOption liActive";
        } else {
            document.getElementsByClassName("leftDesktopOption")[i].className = "leftDesktopOption";
        }
    }

}

function middleTopInActive()
{
    let middleTopOptions = document.getElementsByClassName("middleTopDesktopOption");

    Array.from(middleTopOptions).forEach((element) => {
        if(element == this) {
            element.className = "middleTopDesktopOption"
        } else {
            element.className = "middleTopDesktopOption middleTopInactive";
        }
    })
}

function selectFilter() {

    let filters = document.getElementsByClassName('filter');

    Array.from(filters).forEach((element) => {
        if(element == this) {
            element.className = "filter filterActive";
        } else {
            element.className = "filter";
        }
    })
}